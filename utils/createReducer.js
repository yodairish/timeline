'use strict';

/**
 * Create a reducer function with set of action handlers and an optional
 * initial state
 *
 * @param {object} handlers - List of the action handlers
 * @param {object} initialState - Initial state of the reducer
 * @return {function} - A reducer with binded handlers
 */
export default function createReducer(handlers, initialState={}) {

  /**
   * A reducer with binded action handlers
   * If handlers process a action with provided type than return updated state
   * otherwise return same previous state
   *
   * @param {object} state - Current state of the timeline
   * @param {object} action - Contains instructions to change the data
   * @return {object} - A state with changes
   */
  return (state=initialState, action) => {
    return (handlers[action.type]
              ? handlers[action.type](state, action)
              : state);
  }
}
