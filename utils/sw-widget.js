(() => {
  'use strict';

  const timeline = {};
  const pins = Array.from(document.querySelectorAll('#mw-content-text > ul > li'));
  let prev = null;

  pins.forEach(pin => {
    const dateEvents = getDateAndEvents(pin.childNodes);
    let date = getDateKeyForTimeline(dateEvents.date);

    if (typeof date === 'object') {
      dateEvents.events.push(date.event);
      date = date.date;
    }

    if (!date && prev) {
      date = prev;
    }

    if (timeline[date]) {
      timeline[date] = timeline[date].concat(dateEvents.events);

    } else {
      timeline[date] = dateEvents.events;
    }

    prev = date;
  });

  function getDateAndEvents(elems) {
    const elemsArr = Array.from(elems);

    return elemsArr.reduce((result, child) => {
      if (child.tagName === 'UL') {
        result.events = getEvents(child);

      } else if (child.tagName !== 'SUP') {
        result.date += child.textContent.replace("\n", '');
      }

      return result;
    }, { date: '', events: [] });
  }

  function checkBeforeYavinDate(date) {
    return (date.indexOf('BBY') !== -1
              ? '-'
              : '');
  }

  function getDateKeyForTimeline(date) {
    const beforeYavin = checkBeforeYavinDate(date);
    let event = null;
    const dateKey = date
        .split(String.fromCharCode(8211))
        .reduce((full, part) => {
          const partNumber = getDateNumber(part);
          const isZero = (part.indexOf('0') === 0);

          if (partNumber || isZero) {
            full = appendDatePart({
              date: full,
              part: partNumber,
              beforeYavin: (isZero
                              ? ''
                              : beforeYavin)
            });

          } else if (full) {
            event = part.trim();
          }

          return full;
        }, '');

    return (event
              ? {
                date: dateKey,
                event: event
              }
              : dateKey);
  }

  function getDateNumber(date) {
    return +date.replace(/[^0-9]+/g, '');
  }

  function appendDatePart(data) {
    const beforeYavin = data.beforeYavin;
    let part = data.part;
    let date = data.date;

    part = beforeYavin + part;

    if (date) {
      date += String.fromCharCode(8211);
    }

    date += part;

    return date;
  }

  function getEvents(list) {
    return Array.from(list.children).map(item => {
      const itemText = Array.from(item.childNodes).reduce((text, node) => {
        return (node.tagName !== 'SUP'
                  ? text += node.textContent.replace("\n", '')
                  : text);
      }, '');

      return (itemText.toLowerCase().indexOf('see') === 0
                ? ''
                : itemText);
    }).filter(item => item);
  }

  console.log(timeline);
})();