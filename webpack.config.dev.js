'use strict';

var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var autoprefixer = require('autoprefixer');
var imports = require('postcss-import');
var calc = require('postcss-calc')();
var customProperties = require('postcss-custom-properties')();
var mixins = require('postcss-mixins')();
var autoreset = require('postcss-autoreset')();
var initial = require('postcss-initial');

module.exports = {
  entry: [
    'webpack-hot-middleware/client',
    './client/index.js'
  ],
  output: {
    path: path.join(__dirname, '/public/js'),
    filename: 'bundle.js',
    publicPath: '/public/static/'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        loader: 'style!css?modules&localIdentName=[local]-[hash:base64:5]!postcss'
      }
    ]
  },
  postcss: [
    imports({
      path: path.join(__dirname, '/client/css')
    }),
    autoprefixer({
      browsers: ['last 1 version']
    }),
    mixins,
    customProperties,
    calc,
    autoreset,
    initial({
      reset: 'inherited'
    })
  ],
  node: {
    fs: 'empty'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  resolve: {
    alias: {
      components: path.join(__dirname, '/client/components'),
      containers: path.join(__dirname, '/client/containers'),
      css: path.join(__dirname, '/client/css'),
      utils: path.join(__dirname, '/utils')
    }
  },
  devtool: '#inline-source-map'
};