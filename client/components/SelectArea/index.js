'use strict';

import React, { Component, PropTypes } from 'react';
import styles from './style.css';

const styleTypes = {
  current: styles['select-area__background--current'],
};

const propTypes = {
  width: PropTypes.number.isRequired,
  shiftLeft: PropTypes.number.isRequired,
  type: PropTypes.string,
  layer: PropTypes.number,
  change: PropTypes.shape({
    position: PropTypes.bool,
    edge: PropTypes.bool,
    onUpdate: PropTypes.func.isRequired,
    onFinish: PropTypes.func,
  }),
};

export default class SelectArea extends Component {
  /**
   * Represent area which visualize part of some data.
   * Optionally can change by moving edges or whole area.
   *
   * @param {object=} props - The properties set to the component
   * @constructor
   */
  constructor(props) {
    super(props);

    this.prevPos = 0;
    this.pxWidth = 0;
    this.changeSide = null;
    this.isMoving = false;
    this.containerWidth = this.props.containerWidth;

    this._startMoveArea = this._startMoveArea.bind(this);
    this._startChangeLeft = this._startChangeLeft.bind(this);
    this._startChangeRight = this._startChangeRight.bind(this);
    this._updateArea = this._updateArea.bind(this);
    this._stopChange = this._stopChange.bind(this);
  }

  /**
   * Compare container width and update if needed
   * 
   * @param {object} newProps - New props of component
   */
  componentWillReceiveProps(newProps) {
    const needUpdateContainer = (
      !this.containerWidth ||
      newProps.containerWidth ||
      this.props.width !== newProps.width
    );

    if (needUpdateContainer) {
      this._updateContainerWidth(newProps.containerWidth);
    }
  }

  /**
   * Begins to monitor moving area
   *
   * @param {object} e - Mouse down event
   */
  _startMoveArea(e) {
    this.isMoving = true;
    this._startChange(e);
  }

  /**
   * Begins to monitor moving a mouse for a left edge
   *
   * @param {object} e - Mouse down event
   */
  _startChangeLeft(e) {
    this.changeSide = 'left';
    this._startChange(e);
  }

  /**
   * Begins to monitor moving a mouse for a right edge
   *
   * @param {object} e - Mouse down event
   */
  _startChangeRight(e) {
    this.changeSide = 'right';
    this._startChange(e);
  }

  /**
   * Save current position and begins to monitor moving a mouse.
   * Update size of the area base on the new position
   *
   * @param {object} e - Mouse down event
   */
  _startChange(e) {
    e.preventDefault();
    this.prevPos = e.clientX;

    document.addEventListener('mousemove', this._updateArea);
    document.addEventListener('mouseup', this._stopChange);
  }

  /**
   * Compare horizontal position and if changed than calculate shift of the area
   * in percent of container width
   * Call update callback with changes.
   *
   * @param {object} e - Mouse move event
   */
  _updateArea(e) {
    const newPos = e.clientX;

    if (newPos !== this.prevPos) {
      if (!this.containerWidth) {
        this._updateContainerWidth();
      }

      const isLeftChanged = (this.changeSide === 'left');
      const deltaXPx = newPos - this.prevPos;
      const deltaX = deltaXPx / this.containerWidth;

      this.props.change.onUpdate({
        deltaX,
        width: !!this.changeSide,
        shift: isLeftChanged || this.isMoving,
      });

      this.prevPos = newPos;
    }
  }

  /**
   * Stop monitor moving a mouse.
   * Call finish changing area callback with new values.
   */
  _stopChange() {
    this.changeSide = null;
    this.isMoving = false;
    this.prevPos = 0;
    document.removeEventListener('mousemove', this._updateArea);
    document.removeEventListener('mouseup', this._stopChange);

    if (this.props.change.onFinish) {
      this.props.change.onFinish();
    }
  }

  /**
   * Set new width of container
   * If not pass the value, width will calculated by width of area in px and
   * percent width from the props
   *
   * @param {number=} width - New width of container
   */
  _updateContainerWidth(width) {
    if (!width) {
      const areaWidth = this.refs.area.getDOMNode().offsetWidth;

      width = areaWidth / (this.props.width / 100);
    }
    
    this.containerWidth = width;
  }

  /**
   * Create edge of chosen side.
   * By dragging edge you can change width of the area.
   *
   * @param {string=} side - Side of the edge
   * @return {ReactComponent} - Draggable edge of the area
   */
  _getEdge(side='left') {
    const isRight = (side === 'right');
    const callback = (isRight
                        ? this._startChangeRight
                        : this._startChangeLeft);
    const edgeClasses = [styles['select-area__edge']];

    if (isRight) {
      edgeClasses.push(styles['select-area__edge--right']);
    }

    return (
      <span
        key={ `edge_${ side }` }
        className={ edgeClasses.join(' ') }
        onMouseDown={ callback }
      />
    );
  }

  _getAreaClasses() {
    const classes = [styles['select-area']];

    if (this.props.layer) {
      classes.push(styles['select-area--layer-' + this.props.layer]);
    }

    return classes.join(' ');
  }

  /**
   * Use style for the current type of background of the area
   *
   * @return {string} - List of classes base on current type of the area
   */
  _getBackgroundClasses() {
    const classes = [styles['select-area__background']];

    if (this.props.change && this.props.change.position) {
      classes.push(styles['select-area--movable']);
    }

    if (styleTypes[this.props.type]) {
      classes.push(styleTypes[this.props.type]);
    }

    return classes.join(' ');
  }

  /**
   * Markup of the area.
   * Contains: filled area, edges(optional)
   *
   * If area is changable depending on the props:
   *   - add edges
   *   - made filled area draggable
   *
   * @return {ReactComponent} - Dom representation of selected area
   */
  render() {
    let startMoveArea;
    let edges;

    if (this.props.change) {
      if (this.props.change.position) {
        startMoveArea = this._startMoveArea;
      }

      if (this.props.change.edge) {
        edges = [
          this._getEdge('left'),
          this._getEdge('right'),
        ];
      }
    }

    return (
      <div
        ref="area"
        className={ this._getAreaClasses() }
        style={{
          width: this.props.width + '%',
          left: this.props.shiftLeft + '%',
        }}
        onMouseDown={ startMoveArea }
      >
        <div className={ this._getBackgroundClasses() } />
        { edges }
      </div>
    );
  }
}
