'use strict';

import React, { Component, PropTypes } from 'react';
import ScaleSegment from 'components/ScaleSegment';
import SelectArea from 'components/SelectArea';
import styles from './style.css';

const propTypes = {
  width: PropTypes.number.isRequired,
  dateFrom: PropTypes.number.isRequired,
  dateTo: PropTypes.number.isRequired,
  viewFrom: PropTypes.number.isRequired,
  viewTo: PropTypes.number.isRequired,
  segments: PropTypes.shape({
    width: PropTypes.number.isRequired,
    count: PropTypes.number.isRequired,
    step: PropTypes.number.isRequired,
    shift: PropTypes.number.isRequired,
    startFrom: PropTypes.number.isRequired,
  }).isRequired,
  onZoom: PropTypes.func.isRequired,
  onChangeViewRange: PropTypes.func.isRequired,
  onNewView: PropTypes.func.isRequired,
};

export default class DateSlider extends Component {
  /**
   * Currently scaled part of the timeline with scale segments which
   * showing currently scale.
   *
   * It contains a viewing area which represents a time range for which a user
   * get data
   *
   * User can zoom it/out by scroll
   *
   * |_._._|//////|._|
   *
   * @param {object=} props - The properties set to the slider
   * @constructor
   */
  constructor(props) {
    super(props);

    this._onZoom = this._onZoom.bind(this);
  }

  /**
   * Cancel a default scroll event and forward to parent
   *
   * @param {object} e - scroll event
   */
  _onZoom(e) {
    e.preventDefault();

    this.props.onZoom(e.deltaY);
  }

  /**
   * Generate segment elements to display a current scale on the slider.
   * We use counts and size of segments calculated before by the container width
   * Values is rounded number so we also add a position shift
   *
   * @return {?array} - An array of ScaleSegment with current scale values
   */
  _getSegments() {
    if (!this.props.width) return null;

    const { count, step, width, shift, startFrom } = this.props.segments;
    const segmentsList = [];

    for (let i = 0; i < count; i++) {
      const value = startFrom + (step * i);

      segmentsList.push(
        <ScaleSegment
          key={ `segment_${ i }` }
          width={ width }
          value={ value }
        />
      );
    }

    return (
      <div style={{
        marginLeft: shift,
        width: width * count 
      }}>
        { segmentsList }
      </div>
    );
  }

  /**
   * Calculate size and shift base on viewing and timeline date edges
   * and create SelectArea element with these params
   *
   * @return {ReactComponent} - An element for viewing area with size and
   *         position of current state
   */
  _getSelectArea() {
    const width = Math.abs(
      (this.props.viewTo - this.props.viewFrom) /
      (this.props.dateTo - this.props.dateFrom)
    ) * 100;
    const shift = (
      (this.props.viewFrom - this.props.dateFrom) /
      ((this.props.dateTo - this.props.dateFrom))
    ) * 100;

    return (
      <SelectArea
        width={ width }
        shiftLeft={ shift }
        change={{
          edge: true,
          onUpdate: this.props.onChangeViewRange,
          onFinish: this.props.onNewView,
        }}
      />
    );
  }

  /**
   * Markup of the slider.
   * Contains: edges, scale segments, select area
   *
   * @return {ReactComponent} - Dom representation of the slider
   */
  render() {
    return (
      <div
        className={ styles['date-slider'] }
        ref="slider"
        onWheel={ this._onZoom }
      >
        <span className={ styles['date-slider__edge'] }>
          { this.props.dateFrom }
        </span> 

        { this._getSegments() }
        { this._getSelectArea() }

        <span
          className={[
            styles['date-slider__edge'],
            styles['date-slider__edge--right'],
          ].join(' ')}
        >
          { this.props.dateTo }
        </span>
      </div>
    );
  }
}

DateSlider.propTypes = propTypes;
