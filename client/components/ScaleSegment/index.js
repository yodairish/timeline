'use strict';

import React, { Component, PropTypes } from 'react';
import styles from './style.css';

// Paddin from border and border itself
// Don't know how made aligment without it
const PADDING = 6;

const propTypes = {
  width: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

/**
 * Scaled segment showing a step of the scale
 */
export default class ScaleSegment extends Component {
  /**
   * Markup of the segment.
   * Modify width with a padding constant
   *
   * @return {ReactComponent} - Dom representation of the segment
   */
  render() {
    return (
      <span
        className={ styles['scale-segment'] }
        style={{
          width: (this.props.width - PADDING)
        }}
      >
        { this.props.value }
      </span>
    );
  }
}

ScaleSegment.propTypes = propTypes;
