'use strict';

import React, { Component, PropTypes } from 'react';
import styles from './style.css';
import SelectArea from 'components/SelectArea';

const propTypes = {
  width: PropTypes.number.isRequired,
  dateFrom: PropTypes.number.isRequired,
  dateTo: PropTypes.number.isRequired,
  scaleFrom: PropTypes.number.isRequired,
  scaleTo: PropTypes.number.isRequired,
  viewFrom: PropTypes.number.isRequired,
  viewTo: PropTypes.number.isRequired,
  onMove: PropTypes.func.isRequired,
};

export default class ScaleSlider extends Component {
  /**
   * Full timeline with a representation of currently scaled area on it.
   * Scaled area is movable.
   * Visible only if a user scale timeline.
   *
   * |_____|//////|__|
   *
   * @param {object=} props - The properties set to the component
   * @constructor
   */
  constructor(props) {
    super(props);

    this.dateRange = (this.props.dateTo - this.props.dateFrom);
    this.scaleShift = this._getAreaShift({
      area: this.props.scaleFrom,
      date: this.props.dateFrom,
    });
    this.viewShift = this._getAreaShift({
      area: this.props.viewFrom,
      date: this.props.dateFrom,
    });
  }

  /**
   * Update shift of areas base on new props
   * 
   * @param {object} newProps - New props of component
   */
  componentWillReceiveProps(newProps) {
    this.scaleShift = this._getAreaShift({
      area: newProps.scaleFrom,
      date: newProps.dateFrom,
    });
    this.viewShift = this._getAreaShift({
      area: newProps.viewFrom,
      date: newProps.dateFrom,
    });
  }

  /**
   * Calculate shift of area
   * 
   * @param {object} dates - Begin dates of area and timeline
   * @return {number} - Calculated area shift
   */
  _getAreaShift(dates) {
    const { area, date } = dates;

    return (
      (area - date) /
      this.dateRange
    ) * 100;
  }

  /**
   * Calculate size and shift base on scaled and timeline date edges
   * and create SelectArea element with these params
   *
   * @return {ReactComponent} - area which right now is scaled and in detail
   *          open in DateSlider
   */
  _getScaledArea() {
    const width = Math.abs(
      (this.props.scaleTo - this.props.scaleFrom) /
      this.dateRange
    ) * 100;

    return (
      <SelectArea
        width={ width }
        shiftLeft={ this.scaleShift }
        containerWidth={ this.props.width }
        type="current"
        layer={ 2 }
        change={{
          position: true,
          onUpdate: this.props.onMove,
        }}
      />
    );
  }

  _getViewArea() {
    const width = Math.abs(
      (this.props.viewTo - this.props.viewFrom) /
      this.dateRange
    ) * 100;

    return (
      <SelectArea
        width={ width }
        shiftLeft={ this.viewShift }
        containerWidth={ this.props.width }
      />
    );
  }
 
  /**
   * Markup of the slider.
   * Contains: edges, scaled area
   *
   * @return {ReactComponent} - Dom representation of the slider
   */
  render() {
    return (
      <div className={ styles['scale-slider'] }>
        <span className={ styles['scale-slider__edge'] }>
          { this.props.dateFrom }
        </span>

        { this._getScaledArea() }
        { this._getViewArea() }

        <span
          className={[
            styles['scale-slider__edge'],
            styles['scale-slider__edge--right'],
          ].join(' ')}
        >
          { this.props.dateTo }
        </span>
      </div>
    );
  }
}

ScaleSlider.propTypes = propTypes;
