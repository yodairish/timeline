'use strict';

export const MOVE_ZOOMED_AREA = 'MOVE_ZOOMED_AREA';
export const ZOOM_TIMELINE = 'ZOOM_TIMELINE';
export const CHANGE_VIEW_RANGE = 'CHANGE_VIEW_RANGE';
export const GET_EVENTS_FOR_VIEW_RANGE = 'GET_EVENTS_FOR_VIEW_RANGE';
export const CHANGE_TIMELINE_WIDTH = 'CHANGE_TIMELINE_WIDTH';

/**
 * Update the zoomed area by shift
 * 
 * @param {object} data - Contains a shift delta of the zoomed area
 * @return {object} - Action to move zoomed area
 */
export function moveZoomedArea(data) {
  return {
    deltaX: data.deltaX,
    type: MOVE_ZOOMED_AREA,
  };
};

/**
 * Update scale of the zoomed area
 * 
 * @param {number} scale - Contains a scale change
 * @return {object} - Action to change scale of the zoomed area
 */
export function zoomTimeline(scale) {
  return {
    scale,
    type: ZOOM_TIMELINE,
  };
}

/**
 * Update scale of the zoomed area
 * 
 * @param {object} data - Contains a dates delta of the view area and was it
          affected to position of the area
 * @return {object} - Action to change a view area
 */
export function changeViewRange(data) {
  const { deltaX, shift } = data;

  return {
    deltaX,
    shift,
    type: CHANGE_VIEW_RANGE,
  };
}

/**
 * Update the events by a current view range
 * 
 * @return {object} - Action to update the events list
 */
export function getEvents() {
  return {
    type: GET_EVENTS_FOR_VIEW_RANGE,
  };
}

/**
 * Update width of the timeline
 * 
 * @param {object} width - Contains a new width of the timeline
 * @return {object} - Action to update a width of the timeline
 */
export function setTimelineWidth(width) {
  return {
    width,
    type: CHANGE_TIMELINE_WIDTH,
  }
}
