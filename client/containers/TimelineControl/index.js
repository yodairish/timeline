'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  moveZoomedArea,
  zoomTimeline,
  changeViewRange,
  getEvents,
  setTimelineWidth
} from '../../actions';
import ScaleSlider from 'components/ScaleSlider';
import DateSlider from 'components/DateSlider';

/**
 * The component provides management date range
 */
class TimelineControl extends Component {
  constructor(props) {
    super(props);

    this._updateContainerWidth = this._updateContainerWidth.bind(this);
  }
  
  /**
   * When timeline is mount into the DOM we get his width
   * And monitor to resize of the window
   */
  componentDidMount() {
    this._updateContainerWidth();

    window.addEventListener('resize', this._updateContainerWidth);
  }

  /**
   * Stop monitor to resize of the window
   */
  componentWillUnmount() {
    window.removeEventListener('resize', this._updateContainerWidth);
  }

  /**
   * Set a current width of the container
   */
  _updateContainerWidth() {
    const containerWidth = this.refs.container.getDOMNode().offsetWidth;

    this.props.setTimelineWidth(containerWidth);
  }

  /**
   * If timeline is scaled create a slider to deal with scale
   * 
   * @return {ReactComponent} - Slider with current scale
   */
  _getScaleSlider() {
    if (!this.props.zoom.active) return null;

    return (
      <ScaleSlider
        width={ this.props.width }
        dateFrom={ this.props.dates.from }
        dateTo={ this.props.dates.to }
        scaleFrom={ this.props.zoom.from }
        scaleTo={ this.props.zoom.to }
        viewFrom={ this.props.view.from }
        viewTo={ this.props.view.to }
        onMove={ this.props.moveZoomedArea }
      />
    );
  }

  /**
   * Create DateSlider and if scale than ScaleSlider
   * 
   * @return {ReactComponent} - Dom representation of the timeline
   */
  render() {
    return (
      <div ref="container">
        { this._getScaleSlider() }
        <DateSlider
          width={ this.props.width }
          dateFrom={ this.props.zoom.from }
          dateTo={ this.props.zoom.to }
          viewFrom={ this.props.view.from }
          viewTo={ this.props.view.to }
          segments={ this.props.segments }
          onZoom={ this.props.zoomTimeline }
          onChangeViewRange={ this.props.changeViewRange }
          onNewView={ this.props.getEvents }
        />
      </div>
    );
  }
}

export default connect(state => {
  const { width, dates, zoom, view, segments } = state.timeline.toJS();

  return { width, dates, zoom, view, segments };

}, {
  moveZoomedArea,
  zoomTimeline,
  changeViewRange,
  getEvents,
  setTimelineWidth
})(TimelineControl);
