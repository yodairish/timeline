'use strict';

const EXTRA_SEGMENTS = 1;
export const SEGMENT_WIDTH = 150;

/**
 * Update a segments data by a current state
 * 
 * @param {Immutable.Record} state - Current state of the timeline
 * @return {Immutable.Record} - New state with updated segments data
 */
export function updateSegments(state) {
  return state
    .update(updateSegmentsCount)
    .update(updateSegmentStep)
    .update(normalizeData)
    .update(updateSegmentShift)
    .update(updateSegmentFrom);
}

/**
 * Calculate an approximate number of the segments fit in slider base on width
 * 
 * @param {Immutable.Record} state - Current state of the timeline
 * @return {Immutable.Record} - New state with updated segments count
 */
function updateSegmentsCount(state) {
  return state.setIn(['segments', 'count'],
    Math.floor(state.get('width') / SEGMENT_WIDTH)
  );
}

/**
 * Calculate a segment value step by rounging a maximum date, which is get by
 * ratio of zoom dates delta and approximate number of the segments
 *
 * @param {Immutable.Record} state - Current state of the timeline
 * @return {Immutable.Record} - New state with a new segment value step
 */
function updateSegmentStep(state) {
  const count = state.getIn(['segments', 'count']);
  const delta = state.getIn(['zoom', 'delta']);

  if (!count || !delta) return state;

  const maxDate = Math.floor(delta / count);
  let step = 1;
  let nextNumber = 1;
  let order = 1;

  do {
    step = nextNumber * order;
    order *= 10;
    nextNumber = Math.floor(maxDate / order);

  } while(nextNumber);

  return state.setIn(['segments', 'step'], step);
}

/**
 * Update count of segments by zoom delta of dates and a segment value step
 * and update a width of the segment base on correct count
 *
 * @param {Immutable.Record} state - Current state of the timeline
 * @return {Immutable.Record} - New state with normalized count and width of segments
 */
function normalizeData(state) {
  const delta = state.getIn(['zoom', 'delta']);
  const step = state.getIn(['segments', 'step']);
  let count = state.getIn(['segments', 'count']);
  let segmentWidth = SEGMENT_WIDTH;

  if (count) {
    const datesBetween = delta - 1;

    // incorrect or empty
    if (!step || datesBetween <= 0) {
      count = 0;
      segmentWidth = 0;

    } else {
      count = Math.floor(delta / step);

      const widthAllSegments = ((count * step) / delta) * state.get('width');

      segmentWidth = widthAllSegments / count;

      count += EXTRA_SEGMENTS;
    }
  }

  return state.mergeIn(['segments'], {
    count,
    width: segmentWidth,
  });
}

/**
 * Calculate a shift of the segments by the over part in divide dates and step
 *
 * @param {Immutable.Record} state - Current state of the timeline
 * @return {Immutable.Record} - New state with a new shift of segments
 */
function updateSegmentShift(state) {
  const step = state.getIn(['segments', 'step']);
  const zoomFrom = state.getIn(['zoom', 'from']);
  const delta = state.getIn(['zoom', 'delta']);
  let over = zoomFrom % step;

  // if a date is negate that we need to use over part of step
  // date: 2321, step: 1000 => over: 321
  // date: -2321, step: 1000 => over: 679
  if (over && zoomFrom < 0) {
    over = step + over;
  }

  return state.setIn(['segments', 'shift'],
    -(state.get('width') / delta) * over
  );
}

/**
 * Set a date point for a first segment
 * If we have an over part than for negate dates we just should subtract it,
 * but for positive add an over part of step
 * date: 2321, over: 321 => +(step - 321) 3000
 * date: -2321, over: -321 => -2000
 *
 * @param {Immutable.Record} state - Rendering data of the slider
 * @return {Immutable.Record} - New state with a start point of segment value 
 */
function updateSegmentFrom(state) {
  const step = state.getIn(['segments', 'step']);
  const zoomFrom = state.getIn(['zoom', 'from']);
  const over = zoomFrom % step;
  let startFrom;

  if (over) {
    if (zoomFrom < 0) {
      startFrom = zoomFrom - over;

    } else {
      startFrom = zoomFrom + (step - over);
    }

  } else {
    startFrom = zoomFrom + step;
  }

  return state.setIn(['segments', 'startFrom'], startFrom);
}
