'use strict';

import { updateSegments } from './segments';

const ZOOM_STEP = 200;
const MAX_ZOOM = 500;

/**
 * Spreads edges of the zoom area dates base on zoom step
 * If between dates will less then MAX_ZOOM than set to this value
 *
 * @param {Immutable.Record} state - Current state of the timeline
 * @return {Immutable.Record} - New state with updated range of the zoomed area
 */
export function zoomIn(state) {
  const zoom = state.get('zoom');
  let zoomFrom = zoom.get('from') + ZOOM_STEP;
  let zoomTo = zoom.get('to') - ZOOM_STEP
  const between = Math.abs(zoomTo - zoomFrom);

  if (between < MAX_ZOOM) {
    const delta = (MAX_ZOOM - between) / 2;

    if (zoomTo < zoomFrom) {
      const tmpFrom = zoomFrom;

      zoomFrom = zoomTo;
      zoomTo = tmpFrom;
    }

    zoomFrom -= delta;
    zoomTo += delta;
  }

  const newZoom = zoom.merge({
    from: zoomFrom,
    to: zoomTo,
  });

  return updateSegments(state.set('zoom',
    newZoom.merge({
      delta: getDelta(newZoom),
      active: isZoomed(state.get('dates'), newZoom),
    })
  ));
}

/**
 * Shifts edges of the zoom area dates base on zoom step
 * If one side will be in min/max pos, add rest to other side
 *
 * @param {Immutable.Record} state - Current state of the timeline
 * @return {Immutable.Record} - New state with updated range of the zoomed area
 */
export function zoomOut(state) {
  const zoom = state.get('zoom');
  let zoomFrom = zoom.get('from') - ZOOM_STEP;
  let zoomTo = zoom.get('to') + ZOOM_STEP;
  let datesFrom = state.getIn(['dates', 'from']);
  let datesTo = state.getIn(['dates', 'to']);

  if (zoomFrom < datesFrom) {
    const deltaFrom = Math.abs(zoomFrom - datesFrom);

    zoomFrom = datesFrom;
    zoomTo += deltaFrom;

    if (zoomTo > datesTo) {
      zoomTo = datesTo;
    }

  } else if (zoomTo > datesTo) {
    const deltaTo = Math.abs(zoomTo - datesTo);

    zoomTo = datesTo;
    zoomFrom -= deltaTo;

    if (zoomFrom < datesFrom) {
      zoomFrom = datesFrom;
    }
  }

  const newZoom = zoom.merge({
    from: zoomFrom,
    to: zoomTo,
  });

  return updateSegments(state.set('zoom',
    newZoom.merge({
      delta: getDelta(newZoom),
      active: isZoomed(state.get('dates'), newZoom),
    })
  ));
}

/**
 * Update the zoomed area by shift
 * 
 * @param {Immutable.Record} state - Current state of the timeline
 * @param {number} deltaX - A shift delta of the zoomed area
 * @return {Immutable.Record} - New state with updated range of the zoomed area
 */
export function move(state, deltaX) {
  const zoom = state.get('zoom');
  let datesFrom = state.getIn(['dates', 'from']);
  let datesTo = state.getIn(['dates', 'to']);

  const zoomRange = zoom.get('to') - zoom.get('from');
  const datesRange = datesTo - datesFrom;
  const zoomShift = Math.floor(datesRange * deltaX);
  let zoomFrom = zoom.get('from') + zoomShift;

  if (zoomFrom < datesFrom) {
    zoomFrom = datesFrom;

  } else if (zoomFrom > (datesTo - zoomRange)) {
    zoomFrom = datesTo - zoomRange;
  }

  const zoomTo = zoomFrom + zoomRange;

  const newZoom = zoom.merge({
    from: zoomFrom,
    to: zoomTo,
  });

  return updateSegments(state.set('zoom',
    newZoom.set('delta', getDelta(newZoom))
  ));
}

/**
 * Calculate delta of dates
 * 
 * @param {Immutable.Map} zoom - Dates edges of the zoomed part of the timeline
 * @return {number} - Delta dates of the zoomed area
 */
function getDelta(zoom) {
  return Math.abs(zoom.get('to') - zoom.get('from'));
}

/**
 * Check whether timeline is zoomed
 * 
 * @param {Immutable.Map} dates - Dates edges of the timeline
 * @param {Immutable.Map} zoom - Dates edges of the zoomed part of the timeline
 * @return {boolean} - State of the zoom
 */
function isZoomed(dates, zoom) {
  return (
    (zoom.get('from') !== dates.get('from')) ||
    (zoom.get('to') !== dates.get('to'))
  );
}
