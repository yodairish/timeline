'use strict';

import Immutable from 'immutable';
import {
  MOVE_ZOOMED_AREA,
  ZOOM_TIMELINE,
  CHANGE_VIEW_RANGE,
  GET_EVENTS_FOR_VIEW_RANGE,
  CHANGE_TIMELINE_WIDTH
} from '../../actions';
import createReducer from 'utils/createReducer';
import { updateSegments, SEGMENT_WIDTH } from './segments';
import {
  zoomIn,
  zoomOut,
  move as moveZoom,
} from './zoom';

const initialState = Immutable.Record({
  width: 0,

  // Full timeline range
  dates: Immutable.Map({
    from: -16000,
    to: 9000,
  }),

  // Currently zoomed part
  zoom: Immutable.Map({
    from: -9000,
    to: -5000,
    delta: 4000,
    active: true,
  }),

  // The range that the user views
  view: Immutable.Map({
    from: -8000,
    to: -6000,
    sideSwitched: false,
  }),

  // Scale segments of the timeline
  segments: Immutable.Map({
    width: SEGMENT_WIDTH,
    count: 0,
    step: 0,
    shift: 0,
    startFrom: 0,
  }),
});

const handlers = {

  /**
   * Update the zoomed area by shift
   * 
   * @param {Immutable.Record} state - Current state of the timeline
   * @param {object} action - Contains a shift delta of the zoomed area
   * @return {Immutable.Record} - New state with updated range of the zoomed area
   */
  [MOVE_ZOOMED_AREA](state, action) {
    return moveZoom(state, action.deltaX);
  },

  /**
   * Update scale of the zoomed area
   * 
   * @param {Immutable.Record} state - Current state of the timeline
   * @param {action} action - Contains a scale change
   * @return {Immutable.Record} - New state with updated range of the zoomed area
   */
  [ZOOM_TIMELINE](state, action) {
    if (action.scale > 0) {
      return zoomOut(state);

    } else if (action.scale < 0) {
      return zoomIn(state);
    }

    return state;
  },

  /**
   * Update view area range by shift
   * 
   * @param {Immutable.Record} state - Current state of the timeline
   * @param {action} action - Contains a dates delta of the view area and was it
   *        affected to position of the area
   * @return {Immutable.Record} - New state with updated range of the view area
   */
  [CHANGE_VIEW_RANGE](state, action) {
    const zoomFrom = state.getIn(['zoom', 'from']);
    const zoomTo = state.getIn(['zoom', 'to']);
    let sideSwitched = state.getIn(['view', 'sideSwitched']);
    let viewFrom = state.getIn(['view', 'from']);
    let viewTo = state.getIn(['view', 'to']);
    const scaleRange = zoomTo - zoomFrom;
    const viewShift = Math.floor(scaleRange * action.deltaX);

    if ((action.shift && !sideSwitched) || (!action.shift && sideSwitched)) {
      viewFrom += viewShift; 

    } else {
      viewTo += viewShift;
    }

    if (viewTo < viewFrom) {
      const tmpTo = viewTo;

      viewTo = viewFrom;
      viewFrom = tmpTo;
      sideSwitched = !sideSwitched;
    }

    return state.mergeIn(['view'], {
      sideSwitched,
      from: viewFrom,
      to: viewTo,
    });
  },

  /**
   * When we use current view area, we accept current state of the view sides
   * 
   * @param {Immutable.Record} state - Current state of the timeline
   * @return {Immutable.Record} - New state with unflaged view side switched
   */
  [GET_EVENTS_FOR_VIEW_RANGE](state) {
    return state.setIn(['view', 'sideSwitched'], false);
  },

  /**
   * Set a current width of the container
   *
   * @param {Immutable.Record} state - Current state of the timeline
   * @param {action} action - Contains a new width of the timeline
   * @return {Immutable.Record} - New state with updated width of the timeline
   */
  [CHANGE_TIMELINE_WIDTH](state, action) {
    return updateSegments(
      state.set('width', action.width)
    );
  },
};

export default createReducer(handlers, new initialState());
