'use strict';

import React from 'react';
import { Provider } from 'react-redux';
import TimelineControl from 'containers/TimelineControl'
import store from './store';

React.render(
  <Provider store={ store }>
    { () => <TimelineControl /> }
  </Provider>,
  document.body
);
